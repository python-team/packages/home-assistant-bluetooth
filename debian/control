Source: home-assistant-bluetooth
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Edward Betts <edward@4angle.com>,
Section: python
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 pybuild-plugin-pyproject,
 python3-all,
 python3-bleak (>= 0.19.0) <!nocheck>,
 python3-habluetooth (>= 3.0) <!nocheck>,
 python3-poetry-core,
 python3-pytest <!nocheck>,
 python3-pytest-cov <!nocheck>,
 python3-setuptools,
Rules-Requires-Root: no
Standards-Version: 4.7.0
Homepage: https://github.com/home-assistant-libs/home-assistant-bluetooth
Vcs-Browser: https://salsa.debian.org/python-team/packages/home-assistant-bluetooth
Vcs-Git: https://salsa.debian.org/python-team/packages/home-assistant-bluetooth.git

Package: python3-home-assistant-bluetooth
Architecture: any
Depends:
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
Description: Home Assistant Bluetooth Models and Helpers
 This library is for accessing Home Assistant Bluetooth models. Libraries use
 these models to receive and parse Bluetooth advertisement data.
 .
 The data used to populate BluetoothServiceInfo comes from bleak's BLEDevice
 and AdvertisementData, except for the 'source' field, which comes from Home
 Assistant and represents the source of the data.
 .
 In the future, Home Assistant may support remote Bluetooth transceivers, which
 may use the source field to determine which device is closest.
